# freemydig : Free my digital life  / libérer ma vie numérique / çlironi jetën tuaj dixhitale / cifereca liberigo

## EN
Digital freedom project, un-GAFAM-ize our digital life. 
GAFAM = Google Apple Facebook Amazon Microsoft.
Here you will find slipers, flyers together with other paper based material to 
run workshops on the subject. A 
[common knowledge base](https://hackmd.io/uK1GMiexR-6yp9wLujkrvA) about digital 
freedom serves as a reference guide for mentors who organize the workshop. This 
common knowledge base will soon be available in a wiki in different languages.


For the moment the project is mostly in french, but English translation is 
coming, together with an Esperanto and an Albanian version. When translated, 
every link used is adapted to the language and to the countries where this 
language is spoken, because things can differ, and because we try to use as 
much as possible local and decentralized sevices. This explains the differences 
that one can notice between the slips and the knowledge base from a language to 
another.

## FR
Comment libérer sa vie numérique : dé-GAFAM-isation. 
GAFAM = Google Apple Facebook Amazon Microsoft
On trouve dans ce projet différents supports à imprimer, tracts et fiches de 
travail pour  pour libérer sa vie numérique et mener des ateliers de 
dé-GAFAM-isation. On y trouve également une 
[base de connaissance commune](https://hackmd.io/uK1GMiexR-6yp9wLujkrvA) qui 
sers de guide de référence aux mentors et organisateurs des ateliers.

## SQ
Versioni ne shqip vjen së shpejti.
Si të çlirojmë jeten tonë dixhitale : ç-GAFAM-izim.
GAFAM = Google Apple Facebook Amazon Microsoft
Ky projekt përmban fletë pune / trakte për ateliere çlirimi te jetës dixhitale 
(cryptoparty). Gjithashtu një 
[baze njohurish](https://hackmd.io/uK1GMiexR-6yp9wLujkrvA) shërben si guidë 
referencë per mentorët dhe drejtuesit e atelierëve.

## EO
Kiel liberigi kaj sekurigi sian ciferan vivon? Kiel mal-GAFAM-igi kaj trovi 
etikajn alternativojn ? 
Oni trovas ĉi tie instruciojn en la formo de gvidfolioj pri alternativoj por 
uzi dum atelieroj de cifereca liberigo aŭ kiel reklamiloj dum eventoj. Oni 
trovas ankaŭ [katalogon](https://hackmd.io/uK1GMiexR-6yp9wLujkrvA) de informoj 
por helpi kaj gvidi organizantoj de tiuj atelieroj. 